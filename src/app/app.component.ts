import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'un ejemplo de como usar Angular';
    empresa = 'www.thecoderun.com.ve';

}
